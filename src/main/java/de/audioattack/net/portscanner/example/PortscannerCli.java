package de.audioattack.net.portscanner.example;

import de.audioattack.io.ConsoleCreator;
import de.audioattack.io.ConsolePrintDecorator;
import de.audioattack.net.portscanner.PortScanner;
import de.audioattack.net.portscanner.dictionary.IPortDictionary;
import de.audioattack.net.portscanner.dictionary.ProtocolInfo;
import de.audioattack.net.portscanner.dictionary.tcp.NmapTcpDictionary;
import de.audioattack.net.portscanner.iterator.DictionaryPortIterator;
import de.audioattack.net.portscanner.listener.IProgressListener;
import de.audioattack.net.portscanner.listener.IScannerStatusListener;
import de.audioattack.net.portscanner.scan.enums.PortResult;
import de.audioattack.net.portscanner.scan.enums.TransportProtocol;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class PortscannerCli {

    private static final ConsolePrintDecorator CONSOLE = new ConsolePrintDecorator(ConsoleCreator.console());

    private static final IPortDictionary PORT_DICTIONARY = new NmapTcpDictionary();

    private final Map<String, List<Integer>> myMap = new ConcurrentHashMap<>();

    private final IScannerStatusListener myStatusListener = new IScannerStatusListener() {

        @Override
        public void onScanStarted(final UUID jobId) {
            CONSOLE.println("Scan started: " + jobId);
        }

        @Override
        public void onScanFinished(final UUID jobId) {
            CONSOLE.println("\nScan finished: " + jobId);
        }

        @Override
        public void onScanAborted(final UUID jobId) {
            // CONSOLE.println("Scan aborted: " + jobId);
        }

        @Override
        public void onHostFound(final UUID jobId, final String host) {
            myMap.put(host, new CopyOnWriteArrayList<>());
        }

        @Override
        public void onPortResult(final UUID jobId, final String host, final int port, final PortResult result,
                                 final TransportProtocol protocol) {
            if (result != PortResult.CLOSED) {
                myMap.get(host).add(port);
            }
        }
    };

    private final IProgressListener myProgressListener = new IProgressListener() {

        private int finished;

        @Override
        public void onJobsFinishedProgress(UUID jobId, float progress) {

            int newFinished = Math.round(progress * 100);
            if (newFinished != finished) {
                finished = newFinished;
                printProgress();
            }
        }

        private void printProgress() {
            CONSOLE.print("\rFINISHED " + finished + "%");
        }
    };

    private void scan() throws UnknownHostException {

        final PortScanner scanner = new PortScanner(myStatusListener, myProgressListener);

        scanner.scan(null, TransportProtocol.TCP, InetAddress.getByName("192.168.1.1"), 24, true, true, false, true,
                new DictionaryPortIterator(PORT_DICTIONARY));

        printResults();
    }

    private void printResults() {

        for (Entry<String, List<Integer>> entry : myMap.entrySet()) {

            CONSOLE.println(entry.getKey());
            for (final int port : entry.getValue()) {
                final List<ProtocolInfo> infos = PORT_DICTIONARY.getProtocolInfo(port);
                CONSOLE.println("\t" + port + "\t" + getNamesAndDescriptions(infos));
            }
        }
    }

    private static String getNamesAndDescriptions(final List<ProtocolInfo> infos) {

        final StringBuilder sb = new StringBuilder();

        for (final ProtocolInfo info : infos) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(info.getName()).append(" (").append(info.getDescription()).append(')');
        }

        return sb.toString();
    }

    /**
     * Used for testing. Ignore it if you don't need it.
     *
     * @param args will be ignored
     * @throws UnknownHostException if IP address is of illegal length
     */
    public static void main(final String[] args) throws UnknownHostException {

        final long start = System.currentTimeMillis();

        new PortscannerCli().scan();

        CONSOLE.println("time: " + (System.currentTimeMillis() - start) + " ms");
    }
}
