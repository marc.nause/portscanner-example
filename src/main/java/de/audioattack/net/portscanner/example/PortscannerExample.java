package de.audioattack.net.portscanner.example;

import de.audioattack.io.ConsoleCreator;
import de.audioattack.io.ConsolePrintDecorator;
import de.audioattack.net.portscanner.PortScanner;
import de.audioattack.net.portscanner.dictionary.IPortDictionary;
import de.audioattack.net.portscanner.dictionary.ProtocolInfo;
import de.audioattack.net.portscanner.dictionary.tcp.NmapTcpDictionary;
import de.audioattack.net.portscanner.iterator.PortRange;
import de.audioattack.net.portscanner.iterator.PortRangeIterator;
import de.audioattack.net.portscanner.listener.IScannerStatusListener;
import de.audioattack.net.portscanner.scan.enums.PortResult;
import de.audioattack.net.portscanner.scan.enums.TransportProtocol;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.UUID;

public class PortscannerExample {

    private static final ConsolePrintDecorator CONSOLE = new ConsolePrintDecorator(ConsoleCreator.console());

    private static final IPortDictionary tcpPorts = new NmapTcpDictionary();
    private static final IPortDictionary udpPorts = new NmapTcpDictionary();

    /**
     * Used for testing. Ignore it if you don't need it.
     *
     * @param args will be ignored
     * @throws UnknownHostException if IP address is of illegal length
     */
    public static void main(final String[] args) throws UnknownHostException {

        final long start = System.currentTimeMillis();

        final PortScanner scanner = new PortScanner(16, new IScannerStatusListener() {

            public void onScanStarted(final UUID jobId) {
                CONSOLE.println("Scan started: " + jobId);
            }

            public void onScanFinished(final UUID jobId) {
                CONSOLE.println("Scan finished: " + jobId);
            }

            public void onScanAborted(final UUID jobId) {
                CONSOLE.println("Scan aborted: " + jobId);
            }

            public void onHostFound(final UUID jobId, final String host) {
                CONSOLE.println("Host found: " + host);
            }

            @Override
            public void onPortResult(final UUID jobId, final String host, final int port, final PortResult result,
                                     final TransportProtocol protocol) {
                if (result != PortResult.CLOSED) {
                    CONSOLE.println("Host: " + host + " "
                            + protocol + " port: " + port + " " + (protocol == TransportProtocol.TCP
                            ? getNames(tcpPorts.getProtocolInfo(port)) : getNames(udpPorts.getProtocolInfo(port)))
                            + " " + result);
                }
            }
        });

        scanner.scan(null, TransportProtocol.TCP, InetAddress.getByName("192.168.1.1"),
                new PortRangeIterator(new PortRange(1, 9000)));

        CONSOLE.println("time: " + (System.currentTimeMillis() - start) + " ms");
    }

    private static String getNames(final List<ProtocolInfo> infos) {

        if (infos == null) {
            return "";
        }

        final StringBuilder sb = new StringBuilder();

        for (final ProtocolInfo info : infos) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(info.getName());
        }

        return sb.toString();
    }
}
